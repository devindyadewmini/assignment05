#include <stdio.h>

int Att (int TP) //This Funtion is for get the Number of Attendees.
{
    return 120-(TP-15)/5*20;
}

int rev (int TP) //This Funtion is for get Revenue.
{
    return TP * Att(TP);
}

int cost (int TP) //This Funtion is for get Cost.
{
    return 500 + (Att(TP) * 3);
}

int pro (int TP) //This Funtion is for get Profit.
{
    return (rev(TP) - cost(TP));
}

int main ()
{
    int TP;
    printf(" \t \t \t \t Profit Summery according to Ticket Price \n \n");
    printf("______________________________________________________________________________________________________ \n \n \n");
    for (TP=45; TP>0; TP=TP-5)
    {
        printf("Price of a Ticket - Rs.%d.00 \t \t <---------------> \t \t Expected Profit = Rs.%d.00 \n \n", TP,pro(TP));
        printf("\n \n");
    }
    printf("________________________________________________________________________________________________________ \n \n \n");
    return 0;
}
